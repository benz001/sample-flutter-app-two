import 'package:flutter/material.dart';
import 'package:sample_flutter_app/ui/choose_screen.dart';
import 'package:sample_flutter_app/ui/events_screen.dart';
import 'package:sample_flutter_app/ui/guests_screen.dart';
import 'package:sample_flutter_app/ui/welcome_screen.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Flutter App",
    initialRoute: "/",
    routes: {
      "/" : (context) => WelcomeScreen(),
      ChooseScreen.routeName : (context) => ChooseScreen(),
      EventsScreen.routeName :(context) => EventsScreen(),
      GuestsScreen.routeName: (context) => GuestsScreen()
    },
  ));
}