import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sample_flutter_app/parameter_screen.dart';
import 'package:sample_flutter_app/ui/choose_screen.dart';

class GuestsScreen extends StatefulWidget {
  static const String routeName = "/guestsScreen";
  const GuestsScreen({Key key}) : super(key: key);

  @override
  _GuestsScreenState createState() => _GuestsScreenState();
}

class _GuestsScreenState extends State<GuestsScreen> {
  static const String routeName = "/guestsScreen";
  List<dynamic> users = List<dynamic>();
  int page = 8;
  ScrollController _controller;
  bool _isLoading = true;
  bool _visibleButton = false;
  bool _isError = false;

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    this._fecthDataUsers();
    super.initState();
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      // _fecthNewDataUsers();
      setState(() {
        _visibleButton = true;
      });
      // print(page++);
    }
  }

  Future<List<dynamic>> _fecthDataUsers() async {
    final String apiUrl =
        "https://reqres.in/api/users?per_page=" + page.toString();
    var result = await http.get(apiUrl);
    var jsonResponse = json.decode(result.body)['data'];
    if (result.statusCode == 200) {
      print("get");
      setState(() {
        _isLoading = false;
        users = jsonResponse;
      });
    } else {
      setState(() {
        _isError = true;
      });
    }
  }

  Future<List<dynamic>> _fecthNewDataUsers() async {
    setState(() {
      page = page + 1;
    });
    print(page);
    final String apiUrl =
        "https://reqres.in/api/users?per_page=" + page.toString();
    var result = await http.get(apiUrl);
    var jsonResponse = json.decode(result.body)['data'];
    print("get");
    setState(() {
      users = jsonResponse;
    });
  }

   showAlertDialog(BuildContext context, String title, String content) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  String checkPhoneUser(int id) {
    if (id % 2 == 0) {
      return "Blackberry";
    } else if (id % 3 == 0) {
      return "Android";
    } else if (id % 2 == 0 && id % 3 == 0) {
      return "iOS";
    } else {
      return "iOS";
    }
  }

  String _checkPrimeNumber(int input) {
    int number, check = 0;
    number = input;
    for (int i = 2; i <= number; i++) {
      if (number % i == 0) {
        check++;
      }
    }
    if (check == 1) {
      return "Prime";
    } else {
      return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
           leading: IconButton(
            icon: Image.asset("images/ic_back_white.png",height: 20, width: 20,),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text("GUESTS"),
          backgroundColor: Color.fromRGBO(231, 120, 45, 1),
        ),
        body: _isLoading
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                     CircularProgressIndicator(),
                     Visibility(
                       visible: _isError,
                       child: Text("Connection failed")
                       )
                  ],
                ) 
              )
            : Container(
                child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 30),
                    width: width,
                    height: height * 0.8,
                    child: GridView.builder(
                        controller: _controller,
                        itemCount: users.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            mainAxisSpacing: 3,
                            crossAxisSpacing: 3,
                            childAspectRatio: 1),
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            height: 200,
                            child: Column(
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pushNamed(
                                      context,
                                      ChooseScreen.routeName,
                                      arguments: ParameterScreen(
                                          "", users[index]["first_name"]),
                                    );
                                  },
                                  child: Container(
                                    width: 120,
                                    height: 120,
                                    decoration: BoxDecoration(
                                        color: Colors.grey,
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                          image: NetworkImage(
                                              users[index]["avatar"]),
                                          fit: BoxFit.cover,
                                        )),
                                  ),
                                ),
                                SizedBox(height: 15,),
                                Column(
                                  children: <Widget>[
                                    Center(
                                      child: Text(
                                        users[index]["first_name"] +
                                            " " +
                                            checkPhoneUser(users[index]["id"]),
                                      ),
                                    ),
                                    Center(
                                        child: Text(_checkPrimeNumber(
                                            users[index]["id"])))
                                  ],
                                )
                              ],
                            ),
                          );
                        }),
                  ),
                  Visibility(
                      visible: _visibleButton,
                      child: Container(
                        width: width,
                        height: height * 0.05,
                        // color: Colors.lightBlue,
                        child: ButtonTheme(
                          height: 30,
                          child: RaisedButton(
                              child: Text(
                                "More",
                                style: TextStyle(color: Colors.white),
                              ),
                              color: Color.fromRGBO(231, 120, 45, 1),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              onPressed: () {
                                _fecthNewDataUsers();
                              }),
                        ),
                      ))
                ],
              )));
  }
}
