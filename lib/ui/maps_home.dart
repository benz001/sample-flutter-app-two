import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapsHome extends StatefulWidget {
  static const String routeName = "/mapsHomeScreen";
  @override
  _MapsHomeState createState() => _MapsHomeState();
}

class _MapsHomeState extends State<MapsHome> {
  LatLng myLocation;
  final Map<String, Marker> _marker = {};

  void getCurrentLocation() async{
//    _marker.clear();
    var currentLocation = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high
    );
    setState(() {
      final myMarker = Marker(
          markerId: MarkerId("My Position"),
          position: LatLng(currentLocation.latitude, currentLocation.longitude),
          infoWindow: InfoWindow(
              title: "My Location",
              snippet: "Udacoding"
          )
      );
      _marker['Current Location'] = myMarker;
      myLocation = LatLng(currentLocation.latitude, currentLocation.longitude);
    });
    print("Lat :  ${currentLocation.latitude}");
    print("Lon : ${currentLocation.longitude}");
  }

  void periodicMethod() async{
      if(this.mounted){
        setState(() {
          getCurrentLocation();
        });
      }
  }

  @override
  void initState() {
    super.initState();
    periodicMethod();
  }

  @override
  void dispose() {
    super.dispose();
    periodicMethod();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Google Maps"),
        ),
        body: ListView(
          children: <Widget>[
            // Text("Latitude : ${myLocation.latitude}"),
            // Text("Longitude : ${myLocation.longitude}"),

            Padding(
              padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: GoogleMap(
                  initialCameraPosition: CameraPosition(
                      target: myLocation == null ? LatLng(-6.2973249,106.6956355) : myLocation,
                      zoom: 14.0
                  ),
                  markers: _marker.values.toSet(),
                  gestureRecognizers:
                  <Factory<OneSequenceGestureRecognizer>> [
                    Factory<OneSequenceGestureRecognizer>(
                          () => ScaleGestureRecognizer(),
                    )
                  ].toSet(),
                ),
              ),
            ),

          ],
        )
    );
  }
}