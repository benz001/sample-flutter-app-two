import 'package:flutter/material.dart';
import 'package:sample_flutter_app/arguments.dart';
import 'package:sample_flutter_app/parameter_screen.dart';
import 'package:sample_flutter_app/parameter_name.dart';
import 'package:sample_flutter_app/ui/events_screen.dart';
import 'package:sample_flutter_app/ui/guests_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChooseScreen extends StatefulWidget {
  static const String routeName = "/chooseScreen";
  const ChooseScreen({Key key}) : super(key: key);

  @override
  _ChooseScreenState createState() => _ChooseScreenState();
}

class _ChooseScreenState extends State<ChooseScreen> {
  String userName = "";
  _callName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String name = prefs.getString("name");
    print("this name : $name");
    if (name != "") {
      setState(() {
        userName = name;
      });
    } else {
      setState(() {
        userName = "Username";
      });
    }
  }

  String guestName = "";
  _callGuestName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String guest_name = prefs.getString("guest_name");
    print("this guest name : $guestName");
    if (guest_name != "") {
      setState(() {
        guestName = guest_name;
      });
    } else {
      setState(() {
        guestName = "Choose Guests";
      });
    }
  }

  @override
  void initState() {
    _callName();
    _callGuestName();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ParameterScreen args = ModalRoute.of(context).settings.arguments;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: height * 0.35,
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(223, 223, 223, 1),
                    image: DecorationImage(
                      image: AssetImage("images/bg_bright.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(right: 20, left: 20),
                        // alignment: Alignment.center,
                        height: 100,
                        width: width * 0.4,
                        // color: Colors.red,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Hallo",
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 20),
                              ),
                              Text(
                                userName,
                                style: TextStyle(
                                    color: Color.fromRGBO(231, 120, 45, 1),
                                    fontSize: 20),
                              )
                            ]),
                      ),
                      Container(
                          // color: Colors.lightBlue,
                          // alignment: Alignment.center,
                          height: 100,
                          width: width * 0.6,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                alignment: Alignment.topRight,
                                // width: width * 0.4,
                                  child: Divider(
                                color: Color.fromRGBO(231, 120, 45, 1),
                                height: 5,
                                thickness: 2,
                                indent: 5,
                                endIndent: 5,
                              )),
                               Container(
                                padding: EdgeInsets.only(left: 0),
                                alignment: Alignment.topRight,
                                // width: width * 0.4,
                                  child: Divider(
                                color: Color.fromRGBO(231, 120, 45, 1),
                                height: 25,
                                thickness: 1,
                                indent: 5,
                                endIndent: 5,
                              )),
                               Container(
                                padding: EdgeInsets.only(left: 10),
                                alignment: Alignment.topRight,
                                // width: width * 0.4,
                                  child: Divider(
                                color: Color.fromRGBO(231, 120, 45, 1),
                                height: 1,
                                thickness: 2,
                                indent: 5,
                                endIndent: 5,
                              ))
                              
                            ],
                          ))
                    ],
                  ),
                ),
                SizedBox(
                  height: height * 0.10,
                ),
                Container(
                  width: width,
                  height: height * 0.40,
                  alignment: Alignment.center,
                  padding: EdgeInsets.all(80),
                  child: Image.asset(
                    "images/img_suitmedia.png",
                    fit: BoxFit.contain,
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: height * 0.15,
                  alignment: Alignment.bottomCenter,
                  child: Image.asset(
                    "images/img_bg_bottom.png",
                  ),
                )
              ],
            ),
          ),
          Center(
            child: Container(
              margin: EdgeInsets.only(bottom: 120),
              width: width,
              height: height * 0.2,
              padding: EdgeInsets.only(right: 20, left: 20),
              // color: Colors.limeAccent,
              child: Column(
                children: <Widget>[
                  ButtonTheme(
                    minWidth: width,
                    height: 50,
                    child: RaisedButton(
                        child: Text(
                          "Choose Event ${args.numberEvent}",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Color.fromRGBO(231, 120, 45, 1),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onPressed: () {
                          Navigator.pushNamed(context, EventsScreen.routeName);
                        }),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  ButtonTheme(
                    minWidth: width,
                    height: 50,
                    child: RaisedButton(
                        child: Text(
                          args.guestName,
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Color.fromRGBO(231, 120, 45, 1),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onPressed: () {
                          Navigator.pushNamed(context, GuestsScreen.routeName);
                        }),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
