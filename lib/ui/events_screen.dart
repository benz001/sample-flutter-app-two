import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:sample_flutter_app/parameter_screen.dart';
import 'package:sample_flutter_app/ui/choose_screen.dart';

class EventsScreen extends StatefulWidget {
  static const String routeName = "/eventsScreen";
  const EventsScreen({Key key}) : super(key: key);

  @override
  _EventsScreenState createState() => _EventsScreenState();
}

class _EventsScreenState extends State<EventsScreen> {
  final String apiUrl = "https://reqres.in/api/users?per_page=15";

  bool mapStatus = false; //off

  LatLng myLocation;
  final Map<String, Marker> _marker = {};

  String firstIcon = "images/ic_map_view.png";
  String secondIcon = "images/ic_list_view.png";

  void getCurrentLocation() async {
//    _marker.clear();
    var currentLocation = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      final myMarker = Marker(
          markerId: MarkerId("My Position"),
          position: LatLng(currentLocation.latitude, currentLocation.longitude),
          infoWindow: InfoWindow(title: "My Location", snippet: "Udacoding"));
      _marker['Current Location'] = myMarker;
      myLocation = LatLng(currentLocation.latitude, currentLocation.longitude);
    });
    print("Lat :  ${currentLocation.latitude}");
    print("Lon : ${currentLocation.longitude}");
  }

  void periodicMethod() async {
    if (this.mounted) {
      setState(() {
        getCurrentLocation();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    periodicMethod();
  }

  @override
  void dispose() {
    super.dispose();
    periodicMethod();
  }

  Future<List<dynamic>> _fecthDataUsers() async {
    var result = await http.get(apiUrl);
    return json.decode(result.body)['data'];
  }

  showAlertDialog(BuildContext context, String title, String content) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(content),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  String _currentDate() {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('MMMM d, yyyy');
    final String formatted = formatter.format(now);
    // print(formatted);
    return formatted;
  }

  Widget _listUsersVertical(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: _fecthDataUsers(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text("Connection failed"),
            );
          }
          if (snapshot.connectionState == ConnectionState.done) {
            return ListView.builder(
                padding: EdgeInsets.only(left: 15, right: 15),
                itemCount: snapshot.data.length,
                itemBuilder: (context, int index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        ChooseScreen.routeName,
                        arguments: ParameterScreen(
                            snapshot.data[index]["id"].toString(),
                            "Choose Guest"),
                      );
                    },
                    child: Card(
                      elevation: 5,
                      margin: EdgeInsets.only(top: 15),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 5,
                            child: Container(
                              height: 150,
                              width: 100,
                              decoration: BoxDecoration(
                                  color: Colors.grey,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10)),
                                  image: DecorationImage(
                                    image: NetworkImage(
                                        snapshot.data[index]["avatar"]),
                                    fit: BoxFit.cover,
                                  )),
                              // child: Image.asset("images/naruto.jpg", fit: BoxFit.fill,),
                            ),
                          ),
                          Expanded(
                              flex: 5,
                              child: Container(
                                padding: EdgeInsets.fromLTRB(8, 10, 8, 0),
                                // padding: EdgeInsets.fromLTRB(6, 6, 6, 1),
                                height: 150,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      flex: 30,
                                      child: Text(
                                        snapshot.data[index]["id"].toString(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    Expanded(
                                        flex: 45,
                                        child: Text(snapshot.data[index]
                                                ["first_name"] +
                                            " " +
                                            (snapshot.data[index]
                                                ["last_name"]))),
                                    Expanded(
                                      flex: 25,
                                      child: Container(
                                          // color: Colors.lightBlue,
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                            _currentDate(),
                                            // style: TextStyle(color: Colors.grey),
                                            textAlign: TextAlign.right,
                                          )),
                                    )
                                  ],
                                ),
                              ))
                        ],
                      ),
                    ),
                  );
                });
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget _listUsersHorizontal(BuildContext context) {
    return Stack(
      children: <Widget>[
        ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: GoogleMap(
                  initialCameraPosition: CameraPosition(
                      target: myLocation == null
                          ? LatLng(-6.2973249, 106.6956355)
                          : myLocation,
                      zoom: 14.0),
                  markers: _marker.values.toSet(),
                  gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                    Factory<OneSequenceGestureRecognizer>(
                      () => ScaleGestureRecognizer(),
                    )
                  ].toSet(),
                ),
              ),
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
          height: MediaQuery.of(context).size.height * 0.25,
          child: FutureBuilder(
            future: _fecthDataUsers(),
            builder: (context, AsyncSnapshot snapshot) {
              if (snapshot.hasError) {
                return Center(
                  child: Text("Connection failed"),
                );
              } else if (snapshot.connectionState == ConnectionState.done) {
                return ListView.builder(
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.only(left: 15, right: 15),
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, int index) {
                      return GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(
                              context,
                              ChooseScreen.routeName,
                              arguments: ParameterScreen(
                                  snapshot.data[index]["id"].toString(),
                                  "Choose Guest"),
                            );
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.65,
                            child: Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 5,
                                    child: Container(
                                      width: 100,
                                      decoration: BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10),
                                              bottomLeft: Radius.circular(10)),
                                          image: DecorationImage(
                                            image: NetworkImage(
                                                snapshot.data[index]["avatar"]),
                                            fit: BoxFit.cover,
                                          )),
                                      // child: Image.asset("images/naruto.jpg", fit: BoxFit.fill,),
                                    ),
                                  ),
                                  Expanded(
                                      flex: 5,
                                      child: Container(
                                        padding:
                                            EdgeInsets.fromLTRB(8, 10, 8, 0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Expanded(
                                              flex: 30,
                                              child: Text(
                                                snapshot.data[index]["id"]
                                                    .toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            Expanded(
                                                flex: 45,
                                                child: Text(snapshot.data[index]
                                                        ["first_name"] +
                                                    " " +
                                                    (snapshot.data[index]
                                                        ["last_name"]))),
                                            Expanded(
                                              flex: 25,
                                              child: Container(
                                                  // color: Colors.lightBlue,
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Text(
                                                    _currentDate(),
                                                    // style: TextStyle(color: Colors.grey),
                                                    textAlign: TextAlign.right,
                                                  )),
                                            )
                                          ],
                                        ),
                                      ))
                                ],
                              ),
                            ),
                          ));
                    });
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("EVENTS"),
          leading: IconButton(
            icon: Image.asset(
              "images/ic_back_white.png",
              height: 20,
              width: 20,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          backgroundColor: Color.fromRGBO(231, 120, 45, 1),
          actions: [
            IconButton(
                icon: Image.asset(
                  "images/ic_search_white.png",
                  height: 20,
                  width: 20,
                ),
                onPressed: () {}),
            IconButton(
                icon: Image.asset(
                  mapStatus == false ? firstIcon : secondIcon,
                  height: 20,
                  width: 20,
                ),
                onPressed: () {
                  setState(() {
                    mapStatus == false ? mapStatus = true : mapStatus = false;
                  });
                }),
          ],
        ),
        body: mapStatus == false
            ? _listUsersVertical(context)
            : _listUsersHorizontal(context));
  }
}
